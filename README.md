1.  Copy local.behat.yml.example to local.behat.yml, making sure you're pointed at the base host you intend. 
    You'll use this file to store settings that reflect your local environment, credentials, 
    and other settings which don't belong in the repository.

2.  Run all tests:
    php behat.phar

For more help, visit http://behat.org
